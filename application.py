import HelloAnalyticsService
from flask import Flask, render_template

application = Flask(__name__)

@application.route('/')
def index():
    return render_template('index.html')

@application.route('/users', methods=['GET'])
def userCount():
    scope = ['https://www.googleapis.com/auth/analytics.readonly']
    service = HelloAnalyticsService.get_service('analytics', 'v3', scope, 'client_secrets.json')
    profile = HelloAnalyticsService.get_first_profile_id(service)
    users = HelloAnalyticsService.activeUsers(HelloAnalyticsService.get_results(service, profile))
    return users

if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.debug = True
    application.run()