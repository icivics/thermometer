import math
import sys
import socket
import time
import colorsys
import argparse
from apiclient.discovery import build
import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools
import HelloAnalytics

# Initial setup
IP = sys.argv[1]
PORT = int(sys.argv[2])

top_of_therm = 2000
act_users = 1000
length = 50
rgbs = [(1, 0, 0) for i in range(length)]
xs = [length / 2 - abs(x - length / 2) for x in range(length)]

##Google Analytics setup
scope = ['https://www.googleapis.com/auth/analytics.readonly']

# Authenticate and construct service.
service = HelloAnalytics.get_service('analytics', 'v3', scope, 'client_secrets.json')
profile = HelloAnalytics.get_first_profile_id(service)

last_time = 0

# Start listening
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((IP, PORT))
print("Network is setup. Broadcasting to " + IP + ":" + str(PORT))
sfile = s.makefile()


def scale(factor):
    def f(x):
        return factor * x

    return f


scale_hex = scale(255)


def value(h):
    def f(x):
        return max(0, min(0.5, h - x))

    return f


def thermometer(height, exp_max=3000.0):
    frac_height = height / exp_max
    h = frac_height * length / 2.0
    value_f = value(h)

    moving_rainbow = lambda x: (float(-x) / length + time.time() / 60) % 1

    cols = [colorsys.hsv_to_rgb(moving_rainbow(x), 1, value_f(x)) for x in xs]
    return (cols)


def donation(t):
    if t < 1 / 3.0:
        t = 3 * t
        f = value(t * length / 2.0)
        cols = [colorsys.hsv_to_rgb((5 * t) % 1, 1, 2 * f(x)) for x in xs]
        return cols
    else:
        t = (t - 1 / 3.0) * 3 / 2.0 * 20
        it = int(t) % 2
        cols = [colorsys.hsv_to_rgb(t % 1, 1, it) for x in xs]
        return cols


for ts in sfile:
    try:
        # TODO get data from GA
        # rgbs = [colour.Color(hsl=(((t + time.time()) % length)/length,1,0.5)) for t in range(length)]
        if (time.time() - last_time > 60):
            b = act_users
            try:
                act_users = HelloAnalytics.activeUsers(HelloAnalytics.get_results(service, profile))
            except Exception as e:
                print(e)
                act_users = b
            act_users = float(act_users)
            last_time = time.time()

            print(act_users)
        # act_users = 3000

        rgbs = thermometer(act_users, top_of_therm)
        # rgbs = donation((time.time() % 10)/10)
        rgbs.extend([(0, 0, 0) for i in range(100)])
        rgbs = [map(scale_hex, rgb) for rgb in rgbs]
        cols = ["#%02x%02x%02x" % (r, g, b) for r, g, b in rgbs]

        s.sendall((''.join(cols) + "\n").encode())
    except Exception as e:
        sfile.close()
        s.close()
        print(e)
        break

sfile.close()
s.close()
