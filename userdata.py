import HelloAnalytics
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    scope = ['https://www.googleapis.com/auth/analytics.readonly']
    service = HelloAnalytics.get_service('analytics', 'v3', scope, 'client_secrets.json')
    profile = HelloAnalytics.get_first_profile_id(service)
    act_users = HelloAnalytics.activeUsers(HelloAnalytics.get_results(service, profile))
    return act_users


if __name__ == "__main__":
    app.run()
